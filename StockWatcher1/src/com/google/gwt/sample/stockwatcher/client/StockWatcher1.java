package com.google.gwt.sample.stockwatcher.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.ArrayList;
import java.util.Date;

import org.gwtbootstrap3.client.ui.Label;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.TextBox;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.sample.stockwatcher.client.ModelFlexTable.FlexColumn;

public class StockWatcher1 extends Composite implements EntryPoint, HasText {

	private ArrayList<String> stocks = new ArrayList<String>();
	private static final int REFRESH_INTERVAL = 5000;

	private static StockWatcher1UiBinder uiBinder = GWT.create(StockWatcher1UiBinder.class);

	interface StockWatcher1UiBinder extends UiBinder<Widget, StockWatcher1> {
	}

	public StockWatcher1() {
		initWidget(uiBinder.createAndBindUi(this));

		table.addColumn(new FlexColumn<StockPrice>() {
			@Override
			public Widget createWidget(StockPrice row) {
				return new InlineHTML("<div >" + row.getSymbol() + "</div>");
			}
		}, "Symbol");

		table.addColumn(new FlexColumn<StockPrice>() {
			@Override
			public Widget createWidget(StockPrice row) {
				return new InlineHTML("<div >" + row.getPrice() + "</div>");
			}
		}, "Price");

		table.addColumn(new FlexColumn<StockPrice>() {
			@Override
			public Widget createWidget(StockPrice row) {
				Label lab = new Label();
				lab.setText(Double.toString(row.getChange()));
				return lab;
			}
		}, "Change");

		table.addColumn(new FlexColumn<StockPrice>() {
			@Override
			public Widget createWidget(final StockPrice row) {
				Button remove = new Button("Remove");
				remove.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						int removedIndex = stocks.indexOf(row.getSymbol());
						stocks.remove(removedIndex);
						table.removeRow(row);
					}
				});
				return remove;
			}
		});

		table.setStyleName(0, "watchListHeader");
		table.setStyleName("watchList");
		table.setStyleName(0, 1, "watchListNumericColumn");
		table.setStyleName(0, 2, "watchListNumericColumn");
		table.setStyleName(0, 3, "watchListRemoveColumn");

		Timer refreshTimer = new Timer() {
			@Override
			public void run() {
				refreshWatchList();
			}
		};
		refreshTimer.scheduleRepeating(REFRESH_INTERVAL);
		refreshWatchList();
	}

	@UiField
	Button Addbutton;

	@UiField
	TextBox AddBox;

	@UiField
	ModelFlexTable<StockPrice> table;

	@UiField
	Label lastUpdatedLabel;

	@Override
	public void onModuleLoad() {
		// TODO Auto-generated method stub
		RootPanel.get().add(new StockWatcher1());
		// Setup timer to refresh list automatically.
	}

	@UiHandler("Addbutton")
	// Listen for mouse events on the Add button.
	void onClick(ClickEvent event) {
		addStock();
	}

	@UiHandler("AddBox")
	void onKeyDown(KeyDownEvent event) {
		if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
			addStock();
		}
	}

	public void setText(String text) {
		Addbutton.setText(text);
	}

	public String getText() {
		return Addbutton.getText();
	}
	/**
	 * Add stock to FlexTable. Executed when the user clicks the addStockButton
	 * or presses enter in the newSymbolTextBox.
	 */
	private void addStock() {
		final String symbol = AddBox.getText().toUpperCase().trim();
		AddBox.setFocus(true);

		// Stock code must be between 1 and 10 chars that are numbers, letters,
		// or dots.
		if (!symbol.matches("^[0-9A-Z&#92;&#92;.]{1,10}$")) {
			Window.alert("'" + symbol + "' is not a valid symbol.");
			AddBox.selectAll();
			return;
		}

		AddBox.setText("");

		// TODO Don't add the stock if it's already in the table.
		if (stocks.contains(symbol))
			return;

		stocks.add(symbol);

		table.addRow(new StockPrice(symbol, 0, 0));
		// TODO Get the stock price.
		refreshWatchList();
	}

	private void refreshWatchList() {
		// TODO Auto-generated method stub
		final double MAX_PRICE = 100.0; // $100.00
		final double MAX_PRICE_CHANGE = 0.02; // +/- 2%
		StockPrice[] prices = new StockPrice[stocks.size()];
		for (int i = 0; i < stocks.size(); i++) {
			double price = Random.nextDouble() * MAX_PRICE;
			double change = price * MAX_PRICE_CHANGE * (Random.nextDouble() * 2.0 - 1.0);
			prices[i] = new StockPrice(stocks.get(i), price, change);
		}

		updateTable(prices);
	}

	private void updateTable(StockPrice[] prices) {
		// TODO Auto-generated method stub
		for (int i = 0; i < prices.length; i++) {
			updateTable(prices[i]);
		}
		// // Display timestamp showing last refresh.
		DateTimeFormat dateFormat = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_TIME_MEDIUM);
		lastUpdatedLabel.setText("Last update : " + dateFormat.format(new Date()));
	}

	private void updateTable(StockPrice price) {
		// TODO Auto-generated method stub
		// Make sure the stock is still in the stock table.
		if (!stocks.contains(price.getSymbol())) {
			return;
		}

		int rownum = stocks.indexOf(price.getSymbol()) + 1;
		// Format the data in the Price and Change fields.
		String priceText = NumberFormat.getFormat("#,##0.00").format(price.getPrice());
		NumberFormat changeFormat = NumberFormat.getFormat("+#,##0.00;-#,##0.00");
		String changeText = changeFormat.format(price.getChange());
		String changePercentText = changeFormat.format(price.getChangePercent());

		// Populate the Price and Change fields with new data.
		table.setText(rownum, 1, priceText);
		Label changeWidget = (Label) table.getWidget(rownum, 2);
		changeWidget.setText(changeText + " (" + changePercentText + "%)");

		// Change the color of text in the Change field based on its value.
		String changeStyleName = "noChange";
		if (price.getChangePercent() < -0.1f) {
			changeStyleName = "negativeChange";
		} else if (price.getChangePercent() > 0.1f) {
			changeStyleName = "positiveChange";
		}

		changeWidget.setStyleName(changeStyleName);
	}

}
