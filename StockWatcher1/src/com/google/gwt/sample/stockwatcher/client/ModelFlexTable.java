package com.google.gwt.sample.stockwatcher.client;

import java.util.LinkedList;
import java.util.List;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

public class ModelFlexTable<T> extends Composite {

	public interface FlexColumn<T> {
		Widget createWidget(T row);
	}

	private final FlexTable table;	
	private final List<FlexColumn<T>> columns = new LinkedList<FlexColumn<T>>();	
	private final List<T> rows = new LinkedList<T>();
	
	public ModelFlexTable() {
		table = new FlexTable();
		initWidget(table);
	}
	
	public void addColumn(FlexColumn<T> column) {
		addColumn(column, "");
	}
	
	public void addColumn(FlexColumn<T> column, String name) {
		columns.add(column);
		if (!"".equals(name)) {
			table.setText(0, columns.size()-1, name);
		}
		recreateRows();
	}
	
	public void addRow(T row) {
		rows.add(row);
		appendRowToTable(row);
	}
	
	public void removeRow(T row) {
		int idx = rows.indexOf(row);
		if (idx == -1) {
			return;
		}
		table.removeRow(idx+1);
		rows.remove(row);
	}
	
	public void setRows(List<T> rows) {
		this.rows.clear();
		this.rows.addAll(rows);
		recreateRows();
	}
	
	//added by me
	public void removeRows(){
		this.rows.clear();
	}
	
	public void recreateRows() {
		int rowCount = table.getRowCount();
		for (int row = rowCount-1; row > 0; row--) {
			table.removeRow(row);
		}
		for (T row : rows) {
			appendRowToTable(row);
		}
	}

	private void appendRowToTable(T row) {
		int rowIdx = table.getRowCount();
		int columnIdx = 0;
		for (FlexColumn<T> column : columns) {
			Widget widget = column.createWidget(row);
			table.setWidget(rowIdx, columnIdx, widget);
			columnIdx++;
		}
	}

	public void setText(int row, int column, String text) {
		// TODO Auto-generated method stub
		table.setText(row, column, text);
	}

	public Widget getWidget(int row, int column) {
		// TODO Auto-generated method stub
		return table.getWidget(row, column);
	}

	public void setStyleName(int row, int column, String styleName) {
		// TODO Auto-generated method stub
		table.getCellFormatter().setStyleName(row, column, styleName);
	}

	public void setStyleName(String styleName) {
		// TODO Auto-generated method stub
		table.setStyleName(styleName);
		
	}

	public void setStyleName(int row, String styleName) {
		// TODO Auto-generated method stub
		table.getRowFormatter().setStyleName(row, styleName);
	}
}